﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Models;

namespace Vyx.MiniPad.Services
{
    public class FileService : SingletonBase<FileService>
    {
        LanguageService LanguageService { get; set; }

        private FileService() { LanguageService = LanguageService.Instance; }

        public TreeNode GetTreeNodeForDirectory(string path)
        {
            var node = CreateFileTreeNode(path, FileTreeNodeType.Directory, true);
            return node;
        }

        public List<TreeNode> GetChildren(string path)
        {
            var children = new List<TreeNode>();

            var files = Directory.GetFiles(path);
            var dirs = Directory.GetDirectories(path);

            foreach (var file in files)
            {
                children.Add(CreateFileTreeNode(file, FileTreeNodeType.File, false));
            }

            foreach (var dir in dirs)
            {
                children.Add(CreateFileTreeNode(dir, FileTreeNodeType.Directory, true));
            }

            return children;
        }

        private TreeNode CreateFileTreeNode(string filePath, FileTreeNodeType type, bool generatePlaceholder)
        {
            var lang = LanguageService.GetTextLanguageForExtension(Path.GetExtension(filePath));

            var node = new TreeNode
            {
                Name = filePath,
                Text = Path.GetFileName(filePath),
                ImageIndex = type == FileTreeNodeType.Directory ? 1 : Convert.ToInt32(lang.Language),
                SelectedImageIndex = type == FileTreeNodeType.Directory ? 1 : Convert.ToInt32(lang.Language),
                Tag = new FileTreeNode
                {
                    Path = filePath,
                    Name = Path.GetFileName(filePath),
                    NodeType = type
                },
            };

            if (generatePlaceholder)
            {
                node.Nodes.Add(new TreeNode
                {
                    Name = "placeholder",
                    Text = "Loading...",
                    Tag = new FileTreeNode { NodeType = FileTreeNodeType.Dummy }
                });
            }

            return node;
        }
    }
}
