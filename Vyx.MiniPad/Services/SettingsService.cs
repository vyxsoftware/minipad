﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Properties;

namespace Vyx.MiniPad.Services
{
    public class SettingsService : SingletonBase<SettingsService>
    {
        private SettingsService() { }

        private void SaveSettings()
        {
            Settings.Default.Save();
        }

        public string CurrentStyle
        {
            get => Settings.Default.CurrentStyle;
            set
            {
                Settings.Default.CurrentStyle = value;
                SaveSettings();
            }
        }

        public Point WindowPosition
        {
            get => Settings.Default.WindowPosition;
            set
            {
                Settings.Default.WindowPosition = value;
                SaveSettings();
            }
        }


        public Size WindowSize
        {
            get => Settings.Default.WindowSize;
            set
            {
                Settings.Default.WindowSize = value;
                SaveSettings();
            }
        }

        public bool IsMaximized
        {
            get => Settings.Default.IsMaximized;
            set
            {
                Settings.Default.IsMaximized = value;
                SaveSettings();
            }
        }

        public List<string> OpenFiles
        {
            get => Settings.Default.OpenFiles?.Cast<string>().ToList();
            set
            {
                var files = new System.Collections.Specialized.StringCollection();
                files.AddRange(value.ToArray());
                Settings.Default.OpenFiles = files;
                SaveSettings();
            }
        }

        public int OpenTabIndex
        {
            get => Settings.Default.OpenTabIndex;
            set
            {
                Settings.Default.OpenTabIndex = value;
                SaveSettings();
            }
        }

        public bool GlobalFontEnabled
        {
            get => Settings.Default.GlobalFontEnabled;
            set
            {
                Settings.Default.GlobalFontEnabled = value;
                SaveSettings();
            }
        }

        public bool GlobalFontSizeEnabled
        {
            get => Settings.Default.GlobalFontSizeEnabled;
            set
            {
                Settings.Default.GlobalFontSizeEnabled = value;
                SaveSettings();
            }
        }

        public string GlobalFont
        {
            get => Settings.Default.GlobalFont;
            set
            {
                Settings.Default.GlobalFont = value;
                SaveSettings();
            }
        }

        public int GlobalFontSize
        {
            get => Settings.Default.GlobalFontSize;
            set
            {
                Settings.Default.GlobalFontSize = value;
                SaveSettings();
            }
        }

        public bool ShowWhiteSpace
        {
            get => Settings.Default.ShowWhiteSpace;
            set
            {
                Settings.Default.ShowWhiteSpace = value;
                SaveSettings();
            }
        }

        public bool ShowIndentGuides
        {
            get => Settings.Default.ShowIndentGuides;
            set
            {
                Settings.Default.ShowIndentGuides = value;
                SaveSettings();
            }
        }

        public bool WordWrap
        {
            get => Settings.Default.WordWrap;
            set
            {
                Settings.Default.WordWrap = value;
                SaveSettings();
            }
        }

        public int ZoomFactor
        {
            get => Settings.Default.ZoomFactor;
            set
            {
                Settings.Default.ZoomFactor = value;
                SaveSettings();
            }
        }

        public string OpenFolder
        {
            get => Settings.Default.OpenFolder;
            set
            {
                Settings.Default.OpenFolder = value;
                SaveSettings();
            }
        }
    }
}
