﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Models;

namespace Vyx.MiniPad.Services
{
    public class ThemeService : SingletonBase<ThemeService>
    {
        private List<MiniPadTheme> availableMiniPadThemes;
        private MiniPadTheme currentTheme;
        private SettingsService settingsService;
        private LanguageService languageService;

        public string FontName { get => settingsService.GlobalFontEnabled ? settingsService.GlobalFont : currentTheme.FontName; }
        public int FontSize { get => settingsService.GlobalFontSizeEnabled ? settingsService.GlobalFontSize : currentTheme.FontSize; }

        private ThemeService()
        {
            settingsService = SettingsService.Instance;
            languageService = LanguageService.Instance;
            LoadAvailableThemes();
            currentTheme = availableMiniPadThemes.FirstOrDefault(
                x => x.Name.Equals(settingsService.CurrentStyle))
                ?? availableMiniPadThemes.FirstOrDefault();
        }

        public List<string> GetAvailebleMiniPadThemes()
        {
            return availableMiniPadThemes.Select(x => x.Name).ToList();
        }

        public void SetCurrentMiniPadTheme(string name)
        {
            currentTheme = availableMiniPadThemes.FirstOrDefault(x => x.Name.Equals(name));
        }

        public LanguageStyle GetLanguageStyle(TextLanguage language)
        {
            var langStyle = currentTheme.LanguageStyles.FirstOrDefault(x => x.Lexer.Equals(language.Lexer));
            return langStyle ?? currentTheme.LanguageStyles.FirstOrDefault(x => x.Lexer.Equals(ScintillaNET.Lexer.Null));
        }

        private void LoadAvailableThemes()
        {
            availableMiniPadThemes = new List<MiniPadTheme>();
            string[] themeFiles;
            try
            {
                themeFiles = Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Consts.StylesFolderName), "*.xml");
            }
            catch
            {
                CreateDefaultTheme();
                return;
            }

            foreach (var file in themeFiles)
            {
                try
                {
                    var theme = LoadMiniPadThemeFromXml(File.ReadAllText(file));
                    availableMiniPadThemes.Add(theme);
                }
                catch
                {
                    // Corrupted theme, ignore loading
                }
            }

            if (availableMiniPadThemes.Count == 0)
            {
                CreateDefaultTheme();
            }
        }

        private MiniPadTheme LoadMiniPadThemeFromXml(string xml)
        {
            var style = new MiniPadTheme();

            var styleXml = XDocument.Parse(xml);

            var rootNode = styleXml.Elements().FirstOrDefault(x => x.Name.LocalName.Equals("MiniPadTheme"));
            var fontNode = rootNode.Elements().FirstOrDefault(x => x.Name.LocalName.Equals("Font"));
            var colorsNode = rootNode.Elements().FirstOrDefault(x => x.Name.LocalName.Equals("Colors"));
            var languagesNode = rootNode.Elements().FirstOrDefault(x => x.Name.LocalName.Equals("Languages"));

            style.Name = rootNode.Attribute("Name").Value;
            style.FontName = fontNode.Attribute("Name").Value;

            int fontsize = 10;
            int.TryParse(fontNode.Attribute("Size").Value, out fontsize);
            style.FontSize = fontsize;

            var colors = new Dictionary<string, Color>();

            foreach (var color in colorsNode.Elements())
            {
                int colorValue = 0;
                int.TryParse(color.Attribute("Value").Value.TrimStart('#'),
                    NumberStyles.HexNumber, CultureInfo.InvariantCulture, out colorValue);
                colors.Add(color.Attribute("Name").Value, Color.FromArgb(colorValue));
            }

            style.FontSize = fontsize;
            style.LanguageStyles = new List<LanguageStyle>();

            foreach (var langNode in languagesNode.Elements())
            {
                var langStyle = new LanguageStyle();

                langStyle.Lexer = (ScintillaNET.Lexer)Enum.Parse(typeof(ScintillaNET.Lexer), langNode.Attribute("Name").Value);
                langStyle.Styles = new List<Style>();

                foreach (var styleNode in langNode.Elements())
                {
                    int id = 0;
                    int.TryParse(styleNode.Attribute("Id").Value, out id);

                    langStyle.Styles.Add(new Style
                    {
                        Name = styleNode.Attribute("Name").Value,
                        ID = id,
                        ForegroundColor = colors[styleNode.Attribute("ForegroundColor").Value],
                        BackgroundColor = colors[styleNode.Attribute("BackgroundColor").Value],
                        FontStyle = styleNode.Attribute("FontStyle").Value
                    });
                }

                style.LanguageStyles.Add(langStyle);
            }

            return style;
        }

        private void CreateDefaultTheme()
        {
            var style = new ScintillaNET.Style(null, 0);

            XmlDocument document = new XmlDocument();
            XmlDeclaration xmlDeclaration = document.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement root = document.DocumentElement;
            document.InsertBefore(xmlDeclaration, root);

            // Main
            XmlElement main = document.CreateElement(string.Empty, "MiniPadTheme", string.Empty);
            main.SetAttribute("Name", "Default");
            document.AppendChild(main);

            //Fonts
            XmlElement fontElement = document.CreateElement(string.Empty, "Font", string.Empty);
            fontElement.SetAttribute("Name", "Consolas");
            fontElement.SetAttribute("Size", "10");
            main.AppendChild(fontElement);

            //Colors
            XmlElement colorsElement = document.CreateElement(string.Empty, "Colors", string.Empty);

            XmlElement colorElement1 = document.CreateElement(string.Empty, "Color", string.Empty);
            colorElement1.SetAttribute("Name", "White");
            colorElement1.SetAttribute("Value", "#FFFFFF");
            colorsElement.AppendChild(colorElement1);

            XmlElement colorElement2 = document.CreateElement(string.Empty, "Color", string.Empty);
            colorElement2.SetAttribute("Name", "Black");
            colorElement2.SetAttribute("Value", "#000000");
            colorsElement.AppendChild(colorElement2);

            main.AppendChild(colorsElement);

            //Styles for each language type
            XmlElement langElements = document.CreateElement(string.Empty, "Languages", string.Empty);

            //Default
            XmlElement defaultLangElement = document.CreateElement(string.Empty, "Language", string.Empty);
            defaultLangElement.SetAttribute("Name", "Null");

            XmlElement defaultStyleElement = document.CreateElement(string.Empty, "Style", string.Empty);

            defaultStyleElement.SetAttribute("Name", "Default");
            defaultStyleElement.SetAttribute("Id", "0");
            defaultStyleElement.SetAttribute("ForegroundColor", "Black");
            defaultStyleElement.SetAttribute("BackgroundColor", "White");
            defaultStyleElement.SetAttribute("FontStyle", "Regular");

            defaultLangElement.AppendChild(defaultStyleElement);
            langElements.AppendChild(defaultLangElement);

            var langs = style.GetType().GetNestedTypes();

            foreach (TypeInfo lang in langs)
            {
                XmlElement langElement = document.CreateElement(string.Empty, "Language", string.Empty);
                langElement.SetAttribute("Name", lang.Name);

                // Styles
                foreach (var item in lang.DeclaredFields)
                {
                    XmlElement styleElement = document.CreateElement(string.Empty, "Style", string.Empty);

                    styleElement.SetAttribute("Name", item.Name);
                    styleElement.SetAttribute("Id", item.GetValue(style).ToString());
                    styleElement.SetAttribute("ForegroundColor", "Black");
                    styleElement.SetAttribute("BackgroundColor", "White");
                    styleElement.SetAttribute("FontStyle", "Regular");

                    langElement.AppendChild(styleElement);
                }

                langElements.AppendChild(langElement);
            }
            main.AppendChild(langElements);

            var theme = LoadMiniPadThemeFromXml(document.OuterXml);
            availableMiniPadThemes.Add(theme);
        }
    }
}
