﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Common;

namespace Vyx.MiniPad.Services
{
    public class SearchService : SingletonBase<SearchService>
    {
        private string lastSearch = string.Empty;

        private int lastSearchIndex;

        private SearchService() { }

		public void Find(Scintilla editor, string searchText, bool next, bool incremental)
		{
            if (editor == null)
            {
				return;
            }

			bool first = lastSearch != searchText;

			lastSearch = searchText;
			if (lastSearch.Length > 0)
			{
				if (next)
				{
					// SEARCH FOR THE NEXT OCCURANCE

					// Search the document at the last search index
					editor.TargetStart = lastSearchIndex - 1;
					editor.TargetEnd = lastSearchIndex + (lastSearch.Length + 1);
					editor.SearchFlags = SearchFlags.None;

					// Search, and if not found..
					if (!incremental || editor.SearchInTarget(lastSearch) == -1)
					{
						// Search the document from the caret onwards
						editor.TargetStart = editor.CurrentPosition;
						editor.TargetEnd = editor.TextLength;
						editor.SearchFlags = SearchFlags.None;

						// Search, and if not found..
						if (editor.SearchInTarget(lastSearch) == -1)
						{
							// Search again from top
							editor.TargetStart = 0;
							editor.TargetEnd = editor.TextLength;

							// Search, and if not found..
							if (editor.SearchInTarget(lastSearch) == -1)
							{
								// clear selection and exit
								editor.ClearSelections();
								return;
							}
						}
					}
				}
				else
				{
					// SEARCH FOR THE PREVIOUS OCCURANCE

					// Search the document from the beginning to the caret
					editor.TargetStart = 0;
					editor.TargetEnd = editor.CurrentPosition;
					editor.SearchFlags = SearchFlags.None;

					// Search, and if not found..
					if (editor.SearchInTarget(lastSearch) == -1)
					{
						// Search again from the caret onwards
						editor.TargetStart = editor.CurrentPosition;
						editor.TargetEnd = editor.TextLength;

						// Search, and if not found..
						if (editor.SearchInTarget(lastSearch) == -1)
						{
							// clear selection and exit
							editor.ClearSelections();
							return;
						}
					}
				}

				// Select the occurance
				lastSearchIndex = editor.TargetStart;
				editor.SetSelection(editor.TargetEnd, editor.TargetStart);
				editor.ScrollCaret();
			}
		}
	}
}
