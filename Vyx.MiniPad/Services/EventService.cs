﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Events;

namespace Vyx.MiniPad.Services
{
    public class EventService : SingletonBase<EventService>
    {
        private EventService() { }

        public event SettingsChangedEventHandler SettingsChanged;

        public event FileOpenedEventHandler FileOpened;

        public void OnSettingsChanged(EventArgs e) => SettingsChanged?.Invoke(this, e);

        public void OnFileOpened(FileOpenedEventArgs e) => FileOpened?.Invoke(this, e);
    }
}
