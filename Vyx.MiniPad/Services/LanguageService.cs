﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Models;

namespace Vyx.MiniPad.Services
{
    public class LanguageService : SingletonBase<LanguageService>
    {
        private LanguageService()
        {
            Languages = new List<TextLanguage>()
            {
                new TextLanguage { Name = ".NET Configuration File", Language = Common.Languages.Xaml, Lexer = ScintillaNET.Lexer.Xml, FileNameExtensions = new string[] { "config" } },
                new TextLanguage { Name = "Ada", Language = Common.Languages.Ada, Lexer = ScintillaNET.Lexer.Ada, FileNameExtensions = new string[] { "adb", "ads" } },
                new TextLanguage { Name = "Assembler", Language = Common.Languages.Asm, Lexer = ScintillaNET.Lexer.Asm, FileNameExtensions = new string[] { "asm", "s" } },
                new TextLanguage { Name = "Basic", Language = Common.Languages.Basic, Lexer = ScintillaNET.Lexer.FreeBasic, FileNameExtensions = new string[] { "bas" } },
                new TextLanguage { Name = "Windows Batch", Language = Common.Languages.Batch, Lexer = ScintillaNET.Lexer.Batch, FileNameExtensions = new string[] { "bat", "cmd" } },
                new TextLanguage { Name = "Blitz Basic", Language = Common.Languages.BlitzBasic, Lexer = ScintillaNET.Lexer.BlitzBasic, FileNameExtensions = new string[] { "bas", "ads" } },
                new TextLanguage { Name = "C", Language = Common.Languages.C, Lexer = ScintillaNET.Lexer.Cpp, FileNameExtensions = new string[] { "c" } },
                new TextLanguage { Name = "C++", Language = Common.Languages.Cpp, Lexer = ScintillaNET.Lexer.Cpp, FileNameExtensions = new string[] { "cpp" } },
                new TextLanguage { Name = "C/C++ Header", Language = Common.Languages.Header, Lexer = ScintillaNET.Lexer.Cpp, FileNameExtensions = new string[] { "h", "hpp" } },
                new TextLanguage { Name = "C#", Language = Common.Languages.Cs, Lexer = ScintillaNET.Lexer.Cpp, FileNameExtensions = new string[] { "cs" } },
                new TextLanguage { Name = "C# Project File", Language = Common.Languages.Xaml, Lexer = ScintillaNET.Lexer.Xml, FileNameExtensions = new string[] { "csproj" } },
                new TextLanguage { Name = "CSS", Language = Common.Languages.Css, Lexer = ScintillaNET.Lexer.Css, FileNameExtensions = new string[] { "css" } },
                new TextLanguage { Name = "Fortran", Language = Common.Languages.Fortran, Lexer = ScintillaNET.Lexer.Fortran, FileNameExtensions = new string[] { "f90", "f95", "f03" } },
                new TextLanguage { Name = "HTML", Language = Common.Languages.Html, Lexer = ScintillaNET.Lexer.Html, FileNameExtensions = new string[] { "htm", "html" } },
                new TextLanguage { Name = "Java", Language = Common.Languages.Java, Lexer = ScintillaNET.Lexer.Cpp, FileNameExtensions = new string[] { "java" } },
                new TextLanguage { Name = "JavaScript", Language = Common.Languages.Java, Lexer = ScintillaNET.Lexer.Cpp, FileNameExtensions = new string[] { "js" } },
                new TextLanguage { Name = "JSON", Language = Common.Languages.Json, Lexer = ScintillaNET.Lexer.Json, FileNameExtensions = new string[] { "json" } },
                new TextLanguage { Name = "Lisp", Language = Common.Languages.Lisp, Lexer = ScintillaNET.Lexer.Lisp, FileNameExtensions = new string[] { "lisp" } },
                new TextLanguage { Name = "Lua", Language = Common.Languages.Lua, Lexer = ScintillaNET.Lexer.Lua, FileNameExtensions = new string[] { "lua" } },
                new TextLanguage { Name = "Plain Text", Language = Common.Languages.PlainText, Lexer = ScintillaNET.Lexer.Null, FileNameExtensions = new string[] { "txt" } },
                new TextLanguage { Name = "Pascal", Language = Common.Languages.Pascal, Lexer = ScintillaNET.Lexer.Pascal, FileNameExtensions = new string[] { "pas" } },
                new TextLanguage { Name = "Perl", Language = Common.Languages.Perl, Lexer = ScintillaNET.Lexer.Perl, FileNameExtensions = new string[] { "pl" } },
                new TextLanguage { Name = "PHP", Language = Common.Languages.Php, Lexer = ScintillaNET.Lexer.PhpScript, FileNameExtensions = new string[] { "php" } },
                new TextLanguage { Name = "PowerShell", Language = Common.Languages.PowerShell, Lexer = ScintillaNET.Lexer.PowerShell, FileNameExtensions = new string[] { "ps1" } },
                new TextLanguage { Name = "Python", Language = Common.Languages.Python, Lexer = ScintillaNET.Lexer.Python, FileNameExtensions = new string[] { "py" } },
                new TextLanguage { Name = "R", Language = Common.Languages.R, Lexer = ScintillaNET.Lexer.R, FileNameExtensions = new string[] { "r" } },
                new TextLanguage { Name = "Ruby", Language = Common.Languages.Ruby, Lexer = ScintillaNET.Lexer.Ruby, FileNameExtensions = new string[] { "rb" } },
                new TextLanguage { Name = "SQL", Language = Common.Languages.Sql, Lexer = ScintillaNET.Lexer.Sql, FileNameExtensions = new string[] { "sql" } },
                new TextLanguage { Name = "Visual Basic", Language = Common.Languages.Vb, Lexer = ScintillaNET.Lexer.Vb, FileNameExtensions = new string[] { "vb" } },
                new TextLanguage { Name = "VBScript", Language = Common.Languages.VbScript, Lexer = ScintillaNET.Lexer.VbScript, FileNameExtensions = new string[] { "vbs" } },
                new TextLanguage { Name = "Verilog", Language = Common.Languages.Verilog, Lexer = ScintillaNET.Lexer.Verilog, FileNameExtensions = new string[] { "v", "vl" } },
                new TextLanguage { Name = "XAML", Language = Common.Languages.Xaml, Lexer = ScintillaNET.Lexer.Xml, FileNameExtensions = new string[] { "xaml" } },
                new TextLanguage { Name = "XML", Language = Common.Languages.Xml, Lexer = ScintillaNET.Lexer.Xml, FileNameExtensions = new string[] { "xml" } }
            };
        }

        public List<TextLanguage> Languages { get; }

        /// <summary>
        /// Default language
        /// </summary>
        public TextLanguage DefaultLanguage => Languages.FirstOrDefault(x => x.Language.Equals(Common.Languages.PlainText));

        public TextLanguage GetTextLanguageForExtension(string extension)
        {
            string ex = extension;

            if (extension.StartsWith("."))
            {
                ex = extension.TrimStart('.');
            }

            return Languages.FirstOrDefault(x => x.FileNameExtensions.Contains(ex)) ?? DefaultLanguage;
        }

        public string GetKeywordsForLexer(ScintillaNET.Lexer lexer)
        {
            switch (lexer)
            {
                case Lexer.Container:
                    return string.Empty;
                case Lexer.Null:
                    return string.Empty;
                case Lexer.Ada:
                    return string.Empty;
                case Lexer.Asm:
                    return string.Empty;
                case Lexer.Batch:
                    return string.Empty;
                case Lexer.Cpp:
                    return "class extends implements import interface new case do while else if for in switch throw get set function var try catch finally while with default break continue delete return each const namespace package include use is as instanceof typeof author copy default deprecated eventType example exampleText exception haxe inheritDoc internal link mtasc mxmlc param private return see serial serialData serialField since throws usage version langversion playerversion productversion dynamic private public partial static intrinsic internal native override protected AS3 final super this arguments null Infinity NaN undefined true false abstract as base bool break by byte case catch char checked class const continue decimal default delegate do double descending explicit event extern else enum false finally fixed float for foreach from goto group if implicit in int interface internal into is lock long new null namespace object operator out override orderby params private protected public readonly ref return switch struct sbyte sealed short sizeof stackalloc static string select this throw true try typeof uint ulong unchecked unsafe ushort using var virtual volatile void while where yield";
                case Lexer.Css:
                    return string.Empty;
                case Lexer.Fortran:
                    return string.Empty;
                case Lexer.FreeBasic:
                    return string.Empty;
                case Lexer.Html:
                    return string.Empty;
                case Lexer.Json:
                    return string.Empty;
                case Lexer.Lisp:
                    return string.Empty;
                case Lexer.Lua:
                    return string.Empty;
                case Lexer.Pascal:
                    return string.Empty;
                case Lexer.Perl:
                    return string.Empty;
                case Lexer.PhpScript:
                    return string.Empty;
                case Lexer.PowerShell:
                    return string.Empty;
                case Lexer.Properties:
                    return string.Empty;
                case Lexer.PureBasic:
                    return string.Empty;
                case Lexer.Python:
                    return string.Empty;
                case Lexer.Ruby:
                    return string.Empty;
                case Lexer.Smalltalk:
                    return string.Empty;
                case Lexer.Sql:
                    return string.Empty;
                case Lexer.Vb:
                    return string.Empty;
                case Lexer.VbScript:
                    return string.Empty;
                case Lexer.Verilog:
                    return string.Empty;
                case Lexer.Xml:
                    return string.Empty;
                case Lexer.BlitzBasic:
                    return string.Empty;
                case Lexer.Markdown:
                    return string.Empty;
                case Lexer.R:
                    return string.Empty;
                default:
                    return string.Empty;
            }
        }
    }
}
