﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Services;

namespace Vyx.MiniPad.Common
{
    public interface IViewBase
    {
        EventService EventService { get; }
        SettingsService SettingsService { get; }
        ThemeService ThemeService { get; }
        LanguageService LanguageService { get; }
        SearchService SearchService { get; }
        FileService FileService { get; }
    }
}
