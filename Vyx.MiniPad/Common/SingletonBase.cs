﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Common
{
    public abstract class SingletonBase<T> where T : class
    {
        protected static readonly Lazy<T> instance = new Lazy<T>(() => CreateInstance());

        private static T CreateInstance()
        {
            return Activator.CreateInstance(typeof(T), true) as T;
        }

        public static T Instance => instance.Value;
    }
}
