﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Common
{
    public enum FindViewMode
    {
        Find,
        FindAndReplace
    }

    public enum FileTreeNodeType
    {
        File,
        Directory,
        Dummy
    }
}
