﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Common
{
    public enum Languages
    {
        PlainText = 2,
        Ada,
        Asm,
        Basic,
        Batch,
        C,
        Cpp,
        Cs,
        Css,
        Fortran,
        Header,
        Html,
        Java,
        JavaScript,
        Json,
        Lisp,
        Lua,
        Pascal,
        Perl,
        Php,
        PowerShell,
        Properties,
        PureBasic,
        Python,
        Ruby,
        Smalltalk,
        Sql,
        Vb,
        VbScript,
        Verilog,
        Xaml,
        Xml,
        BlitzBasic,
        Markdown,
        R
    }
}
