﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Services;

namespace Vyx.MiniPad.Common
{
    public class ControlBase : UserControl, IViewBase
    {
        public EventService EventService { get; }

        public SettingsService SettingsService { get; }

        public ThemeService ThemeService { get; }

        public LanguageService LanguageService { get; }

        public SearchService SearchService { get; }

        public FileService FileService { get; }

        public ControlBase() : base()
        {
            EventService = EventService.Instance;
            SettingsService = SettingsService.Instance;
            ThemeService = ThemeService.Instance;
            LanguageService = LanguageService.Instance;
            SearchService = SearchService.Instance;
            FileService = FileService.Instance;

            Load += FormBase_Load;
        }

        protected virtual void InitializeView() { }

        protected virtual void OnClosing() { }

        private void FormBase_Load(object sender, EventArgs e)
        {
            InitializeView();
        }
    }
}
