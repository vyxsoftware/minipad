﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Models;

namespace Vyx.MiniPad.Common
{
    public static class Consts
    {
        public static readonly string WindowTitle = "MiniPad";

        public static readonly string StylesFolderName = "Themes";

        /// <summary>
        /// Default style for a language
        /// </summary>
        public static readonly string DefaultStyle = "Default";

        /// <summary>
        /// Default editor theme
        /// </summary>
        public static readonly string DefaultTheme = "Default";


        public const int NumberMargin = 1;

        public const int BookmarkMargin = 2;

        public const int BookmarkMarker = 2;

        public const int FoldingMargin = 3;
    }
}
