﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Services;

namespace Vyx.MiniPad.Common
{
    public class FormBase : Form, IViewBase
    {
        public EventService EventService { get; }

        public SettingsService SettingsService { get; }

        public ThemeService ThemeService { get; }

        public LanguageService LanguageService { get; }

        public SearchService SearchService { get; }

        public FileService FileService { get; }

        public FormBase() : base()
        {
            EventService = EventService.Instance;
            SettingsService = SettingsService.Instance;
            ThemeService = ThemeService.Instance;
            LanguageService = LanguageService.Instance;
            SearchService = SearchService.Instance;
            FileService = FileService.Instance;

            FormClosing += FormBase_FormClosing;
            Load += FormBase_Load;
        }

        protected virtual void InitializeView() { }

        protected virtual void OnClosing(FormClosingEventArgs e) { }

        private void FormBase_Load(object sender, EventArgs e)
        {
            InitializeView();
        }

        private void FormBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            OnClosing(e);
        }
    }
}
