﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Models
{
    public class LanguageStyle
    {
        public Lexer Lexer { get; set; }

        public List<Style> Styles { get; set; }
    }
}
