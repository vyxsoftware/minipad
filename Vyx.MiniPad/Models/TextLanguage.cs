﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Models
{
    public class TextLanguage
    {
        public string Name { get; set; }

        public Common.Languages Language { get; set; }

        public string[] FileNameExtensions { get; set; }

        public Lexer Lexer { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
