﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Models
{
    public class MiniPadTheme
    {
        public string Name { get; set; }

        public string FontName { get; set; }

        public int FontSize { get; set; }

        public List<LanguageStyle> LanguageStyles { get; set; }
    }
}
