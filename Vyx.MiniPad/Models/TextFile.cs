﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Services;

namespace Vyx.MiniPad.Models
{
    public class TextFile
    {
        public string FileName { get; set; } = String.Empty;

        public string ShortFileName => Path.GetFileName(FileName);

        public bool IsNew { get; set; } = true;

        public bool IsModified { get; set; } = false;

        public TextLanguage TextLanguage { get; set; } = LanguageService.Instance.DefaultLanguage;
    }
}
