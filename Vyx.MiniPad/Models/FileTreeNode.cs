﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vyx.MiniPad.Common;

namespace Vyx.MiniPad.Models
{
    public class FileTreeNode
    {
        public string Name { get; set; }

        public string Path { get; set; }

        public FileTreeNodeType NodeType { get; set; }
    }
}
