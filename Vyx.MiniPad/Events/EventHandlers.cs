﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Events
{
    public delegate void SettingsChangedEventHandler(object sender, EventArgs e);

    public delegate void FileOpenedEventHandler(object sender, FileOpenedEventArgs e);
}
