﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vyx.MiniPad.Events
{
    public class FileOpenedEventArgs : EventArgs
    {
        public string Path { get; set; }
    }
}
