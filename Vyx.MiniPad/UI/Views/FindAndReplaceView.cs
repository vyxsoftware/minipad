﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;

namespace Vyx.MiniPad.UI.Views
{
    public partial class FindAndReplaceView : FormBase
    {
        public Scintilla Editor { get; set; }

        private FindViewMode mode;

        public FindViewMode Mode
        {
            get => mode;
            set
            {
                mode = value;
                UpdateView();
            }
        }

        public FindAndReplaceView()
        {
            InitializeComponent();
            UpdateView();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    Hide();
                    break;
                default:
                    break;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void UpdateView()
        {
            lblReplace.Visible = Mode.Equals(FindViewMode.FindAndReplace);
            txtReplace.Visible = Mode.Equals(FindViewMode.FindAndReplace);
            btnReplace.Visible = Mode.Equals(FindViewMode.FindAndReplace);

            Height = Mode.Equals(FindViewMode.Find) ? 70 : 100;
            Text = Mode.Equals(FindViewMode.Find) ? "Find" : "Find and replace";
        }

        private void SearchPrevious()
        {
            SearchService.Find(Editor, txtSearch.Text, false, false);
        }

        private void SearchNext()
        {
            SearchService.Find(Editor, txtSearch.Text, true, false);
        }

        private void FindAndReplaceView_Activated(object sender, EventArgs e)
        {
            Opacity = 1.0;
        }

        private void FindAndReplaceView_Deactivate(object sender, EventArgs e)
        {
            Opacity = 0.5;
        }

        private void FindAndReplaceView_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void btnSearchPrevious_Click(object sender, EventArgs e)
        {
            SearchPrevious();
        }

        private void btnSearchNext_Click(object sender, EventArgs e)
        {
            SearchNext();
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            Editor?.ReplaceSelection(txtReplace.Text);
            SearchNext();
        }
    }
}
