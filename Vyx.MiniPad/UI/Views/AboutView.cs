﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;

namespace Vyx.MiniPad.UI.Views
{
    public partial class AboutView : FormBase
    {
        public AboutView()
        {
            InitializeComponent();
        }

        protected override void InitializeView()
        {
            lblVersion.Text = $"Version {Assembly.GetEntryAssembly().GetName().Version}";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
