﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Models;

namespace Vyx.MiniPad.UI.Views
{
    public partial class MainWindowView : FormBase
    {
        private Workspace workspace;
        private TextFile currentTextFile;
        private FindAndReplaceView findView;

        private int newCounter = 1;

        private string FontName => ThemeService.FontName;
        private int FontSize => ThemeService.FontSize;
        private List<string> filesToLoad = new List<string>();
        public List<TextFile> LoadedTextFiles { get; set; } = new List<TextFile>();

        private Scintilla CurrentEditor
        {
            get
            {
                if (tabControl.TabPages.Count == 0)
                {
                    return null;
                }

                return (Scintilla)tabControl.SelectedTab.Controls[0];
            }
        }

        public MainWindowView()
        {
            InitializeComponent();
            findView = new FindAndReplaceView();

            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1)
            {
                for (int i = 1; i < args.Length; i++)
                {
                    if (File.Exists(args[i]))
                    {
                        filesToLoad.Add(args[i]);
                    }
                }
            }
        }

        protected override void InitializeView()
        {
            foreach (var language in LanguageService.Languages)
            {
                cbLanguages.Items.Add(language);
            }

            LoadRecentFiles();
            LoadWindowSettings();
            UpdateView();
            UpdateFileStatus();

            foreach (var theme in ThemeService.GetAvailebleMiniPadThemes())
            {
                var menuItem = new ToolStripMenuItem
                {
                    Name = theme,
                    Text = theme
                };

                menuItem.Click += ThemeSelected;

                themeToolStripMenuItem.DropDownItems.Add(menuItem);
            }

            btnShowWhitespace.Checked = SettingsService.ShowWhiteSpace;
            btnShowIndentGuides.Checked = SettingsService.ShowIndentGuides;
            btnWordWrap.Checked = SettingsService.WordWrap;

            EventService.SettingsChanged += EventService_SettingsChanged;
            EventService.FileOpened += EventService_FileOpened;

            welcomeView.NewFile += btnNew_Click;
            welcomeView.OpenFile += btnOpen_Click;
            welcomeView.OpenFolder += btnOpenFolder_Click;
        }

        private void UpdateView()
        {
            if (tabControl.TabPages.Count > 0)
            {
                welcomeView.Visible = false;
                tabControl.Visible = true;

                btnCloseCurrentTab.Enabled = true;
                btnCloseAllTabs.Enabled = true;

                btnZoomIn.Enabled = true;
                btnZoomOut.Enabled = true;

                btnSearch.Enabled = true;
                btnFindAndReplace.Enabled = true;

                btnShowWhitespace.Enabled = true;
                btnShowIndentGuides.Enabled = true;
                btnWordWrap.Enabled = true;
                cbLanguages.Enabled = true;

                bool canCutCopy = CurrentEditor.SelectedText.Length > 0;

                btnCut.Enabled = canCutCopy;
                cutToolStripMenuItem.Enabled = canCutCopy;

                btnCopy.Enabled = canCutCopy;
                copyToolStripMenuItem.Enabled = canCutCopy;

                btnPaste.Enabled = CurrentEditor.CanPaste;
                pasteToolStripMenuItem.Enabled = CurrentEditor.CanPaste;

                btnUndo.Enabled = CurrentEditor.CanUndo;
                undoToolStripMenuItem.Enabled = CurrentEditor.CanUndo;

                btnRedo.Enabled = CurrentEditor.CanRedo;
                redoToolStripMenuItem.Enabled = CurrentEditor.CanRedo;

                lblPosition.Text = $"Ln: {CurrentEditor.CurrentLine}   Col: {CurrentEditor.GetColumn(CurrentEditor.CurrentPosition)}   Pos: {CurrentEditor.CurrentPosition}";

                SetWhitespaceVisibility();
                SetIndentGuides();
                SetWordWrap();
                SetZoom();
            }
            else
            {
                tabControl.Visible = false;
                welcomeView.Visible = workspace == null;

                btnCloseCurrentTab.Enabled = false;
                btnCloseAllTabs.Enabled = false;

                btnZoomIn.Enabled = false;
                btnZoomOut.Enabled = false;

                btnSearch.Enabled = false;
                btnFindAndReplace.Enabled = false;

                btnShowWhitespace.Enabled = false;
                btnShowIndentGuides.Enabled = false;
                btnWordWrap.Enabled = false;
                cbLanguages.Enabled = false;

                btnSave.Enabled = false;
                saveAsToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;

                btnCut.Enabled = false;
                cutToolStripMenuItem.Enabled = false;

                btnCopy.Enabled = false;
                copyToolStripMenuItem.Enabled = false;

                btnPaste.Enabled = false;
                pasteToolStripMenuItem.Enabled = false;

                btnUndo.Enabled = false;
                undoToolStripMenuItem.Enabled = false;

                btnRedo.Enabled = false;
                redoToolStripMenuItem.Enabled = false;
            }

            scMain.Panel1Collapsed = workspace == null;
            btnCloseFolder.Enabled = workspace != null;
            closeFolderToolStripMenuItem.Enabled = workspace != null;

            SetWindowTitle();
        }

        private void UpdateFileStatus()
        {
            SetWindowTitle();

            if (LoadedTextFiles.Count == 0)
            {
                btnSave.Enabled = false;
                btnSaveAll.Enabled = false;
                
                saveToolStripMenuItem.Enabled = false;
                saveAsToolStripMenuItem.Enabled = false;
                saveAllToolStripMenuItem.Enabled = false;

                return;
            }

            // Set the color of the save icon on each tab
            // to reflect if the file has been modified or not.
            // Red for modified, blue for unmodified.

            foreach (TabPage tabPage in tabControl.TabPages)
            {
                var tabTextFile = LoadedTextFiles.First(x => x.FileName.Equals(tabPage.Name));
                tabPage.ImageIndex = tabTextFile.IsModified ? 1 : 0;
            }

            btnSave.Enabled = currentTextFile.IsModified;
            saveToolStripMenuItem.Enabled = currentTextFile.IsModified;
            saveAsToolStripMenuItem.Enabled = currentTextFile.IsModified;

            bool anyFilesToSave = LoadedTextFiles.Where(file => file.IsModified).Any();

            btnSaveAll.Enabled = anyFilesToSave;
            saveAllToolStripMenuItem.Enabled = anyFilesToSave;
        }

        protected override void OnClosing(FormClosingEventArgs e)
        {
            tabControl.SelectedIndexChanged -= tabControl_SelectedIndexChanged;

            SaveWindowSettings();
            SaveOpenFiles();
            //Hide();

            foreach (var textFile in LoadedTextFiles)
            {
                bool result = AskToSaveFile(textFile);

                if (!result)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void ThemeSelected(object sender, EventArgs e)
        {
            ThemeService.SetCurrentMiniPadTheme(((ToolStripMenuItem)sender).Name);
            UpdateAllEditorStyles();
        }

        private void OpenFolder(string path)
        {
            workspace = new Workspace { Path = path };
            folderView.FolderTree = FileService.GetTreeNodeForDirectory(workspace.Path);
            UpdateView();
        }

        private void CloseFolder()
        {
            workspace = null;
            UpdateView();
        }

        private void LoadTextFile(string fileName)
        {
            if (LoadedTextFiles.Count > 0 && LoadedTextFiles.Any(x => x.FileName.Equals(fileName)))
            {
                tabControl.SelectedTab = tabControl.TabPages.Cast<TabPage>().First(x => x.Name.Equals(fileName));
                return;
            }

            var editor = CreateEditor();

            if (!string.IsNullOrEmpty(fileName))
            {
                try
                {
                    editor.Text = File.ReadAllText(fileName);
                    editor.EmptyUndoBuffer();
                }
                catch (Exception)
                {
                    // Failed to load text
                    throw;
                }
            }

            var textFile = new TextFile
            {
                FileName = string.IsNullOrEmpty(fileName) ? $"New {newCounter++}" : fileName,
                IsNew = string.IsNullOrEmpty(fileName),
            };

            if (!string.IsNullOrEmpty(fileName))
            {
                textFile.TextLanguage = LanguageService.GetTextLanguageForExtension(Path.GetExtension(fileName));
            }
            else
            {
                textFile.TextLanguage = LanguageService.DefaultLanguage;
            }

            LoadedTextFiles.Add(textFile);
            currentTextFile = textFile;

            SetEditorStyle(editor, textFile.TextLanguage);

            var tabPage = new TabPage(textFile.ShortFileName);
            tabPage.Name = textFile.FileName;
            tabPage.ImageIndex = 0;
            tabPage.Controls.Add(editor);

            tabControl.TabPages.Add(tabPage);
            tabControl.SelectedTab = tabPage;

            SetLanguageDropDownValue();
            UpdateView();
            UpdateFileStatus();
            ChangeZoom();

            editor.TextChanged += Editor_TextChanged;
            editor.UpdateUI += Editor_UpdateUI;
            editor.ZoomChanged += Editor_ZoomChanged;
            editor.DragDrop += Editor_DragDrop;
            editor.DragOver += Editor_DragOver;

            CurrentEditor.Focus();
        }

        private void CloseEditorTab(TabPage tabPage)
        {
            var textFileToClose = LoadedTextFiles.FirstOrDefault(
                x => x.FileName.Equals(tabPage.Name));

            var closeTab = AskToSaveFile(textFileToClose);

            if (!closeTab)
            {
                return;
            }

            LoadedTextFiles.Remove(textFileToClose);

            ((Scintilla)tabPage.Controls[0]).TextChanged -= Editor_TextChanged;
            ((Scintilla)tabPage.Controls[0]).UpdateUI -= Editor_UpdateUI;
            ((Scintilla)tabPage.Controls[0]).ZoomChanged -= Editor_ZoomChanged;
            ((Scintilla)tabPage.Controls[0]).DragDrop -= Editor_DragDrop;
            ((Scintilla)tabPage.Controls[0]).DragOver -= Editor_DragOver;

            tabControl.TabPages.Remove(tabPage);

            UpdateView();
            UpdateFileStatus();
        }

        private bool AskToSaveFile(TextFile textFile)
        {
            if (!textFile.IsModified)
            {
                return true;
            }

            var result = MessageBox.Show($"Save file {textFile.FileName}?",
                textFile.ShortFileName,
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);

            if (result == DialogResult.Cancel)
            {
                return false;
            }
            else if (result == DialogResult.Yes)
            {
                SaveFile(currentTextFile, false);
                return true;
            }
            else if (result == DialogResult.No)
            {
                return true;
            }

            return true;
        }

        private void SaveFile(TextFile textFile, bool alwaysShowFileSaveDialog)
        {
            var tabToSave = tabControl.TabPages.Cast<TabPage>()
                .FirstOrDefault(x => x.Name.Equals(textFile.FileName));

            if (textFile.IsNew)
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    textFile.FileName = saveFileDialog.FileName;
                }
                else
                {
                    return;
                }
            }

            if (tabToSave != null)
            {
                try
                {
                    File.WriteAllText(textFile.FileName, tabToSave.Controls[0].Text);
                }
                catch (Exception)
                {

                    throw;
                }
            }

            textFile.IsNew = false;
            textFile.IsModified = false;
            tabToSave.Name = textFile.FileName;
            tabToSave.Text = textFile.ShortFileName;
        }

        private Scintilla CreateEditor()
        {
            var editor = new Scintilla();
            editor.Dock = DockStyle.Fill;
            editor.AllowDrop = true;
            editor.WrapMode = WrapMode.None;
            editor.ScrollWidth = 1;
            editor.Zoom = SettingsService.ZoomFactor;

            foreach (var key in new[] { Keys.F, Keys.S, Keys.H, Keys.R })
            {
                editor.ClearCmdKey(Keys.Control | key);
            }

            // Line number margin
            var nums = editor.Margins[Consts.NumberMargin];
            nums.Width = 30;
            nums.Type = MarginType.Number;
            nums.Sensitive = true;
            nums.Mask = 0;

            // Bookmark margin
            var margin = editor.Margins[Consts.BookmarkMargin];
            margin.Width = 20;
            margin.Sensitive = true;
            margin.Type = MarginType.Symbol;
            margin.Mask = (1 << Consts.BookmarkMarker);

            return editor;
        }

        private void ConfigureEditorFolding(Scintilla editor, bool isFoldingEnabled)
        {
            if (isFoldingEnabled)
            {
                editor.Margins[Consts.FoldingMargin].Width = 20;

                // Code folding
                editor.SetProperty("fold", "1");
                editor.SetProperty("fold.compact", "1");

                editor.Margins[Consts.FoldingMargin].Type = MarginType.Symbol;
                editor.Margins[Consts.FoldingMargin].Mask = Marker.MaskFolders;
                editor.Margins[Consts.FoldingMargin].Sensitive = true;

                editor.Markers[Marker.Folder].Symbol = MarkerSymbol.BoxPlus;
                editor.Markers[Marker.FolderOpen].Symbol = MarkerSymbol.BoxMinus;
                editor.Markers[Marker.FolderEnd].Symbol = MarkerSymbol.BoxPlusConnected;
                editor.Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
                editor.Markers[Marker.FolderOpenMid].Symbol = MarkerSymbol.BoxMinusConnected;
                editor.Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
                editor.Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;
                editor.AutomaticFold = (AutomaticFold.Show | AutomaticFold.Click | AutomaticFold.Change);
            }
            else
            {
                editor.Margins[Consts.FoldingMargin].Width = 0;
            }
        }

        private void SetEditorStyle(Scintilla editor, TextLanguage textLanguage)
        {
            var languageStyle = ThemeService.GetLanguageStyle(textLanguage);
            var defaultStyle = languageStyle.Styles.FirstOrDefault(x => x.Name.Equals(Consts.DefaultStyle));

            editor.Lexer = textLanguage.Lexer;

            bool enableFolding = textLanguage.Language != Languages.PlainText;

            editor.StyleResetDefault();

            editor.Styles[ScintillaNET.Style.Default].Font = FontName;
            editor.Styles[ScintillaNET.Style.Default].Size = FontSize;
            editor.Styles[ScintillaNET.Style.Default].BackColor = defaultStyle.BackgroundColor;
            editor.Styles[ScintillaNET.Style.Default].ForeColor = defaultStyle.ForegroundColor;

            editor.Styles[ScintillaNET.Style.LineNumber].BackColor = defaultStyle.BackgroundColor;
            editor.Styles[ScintillaNET.Style.LineNumber].ForeColor = defaultStyle.ForegroundColor;
            editor.Styles[ScintillaNET.Style.IndentGuide].BackColor = defaultStyle.BackgroundColor;
            editor.Styles[ScintillaNET.Style.IndentGuide].ForeColor = defaultStyle.ForegroundColor;

            editor.StyleClearAll();

            foreach (var style in languageStyle.Styles)
            {
                editor.Styles[style.ID].ForeColor = style.ForegroundColor;
            }

            var marker = editor.Markers[Consts.BookmarkMarker];
            marker.Symbol = MarkerSymbol.Bookmark;
            marker.SetBackColor(defaultStyle.BackgroundColor);
            marker.SetForeColor(defaultStyle.ForegroundColor);
            marker.SetAlpha(100);

            editor.SetFoldMarginColor(true, defaultStyle.BackgroundColor);
            editor.SetFoldMarginHighlightColor(true, defaultStyle.BackgroundColor);

            // Set colors for all folding markers
            for (int i = 25; i <= 31; i++)
            {
                editor.Markers[i].SetForeColor(defaultStyle.BackgroundColor);
                editor.Markers[i].SetBackColor(defaultStyle.ForegroundColor);
            }

            ConfigureEditorFolding(editor, enableFolding);
            editor.SetKeywords(0, LanguageService.GetKeywordsForLexer(textLanguage.Lexer));
        }

        private void UpdateAllEditorStyles()
        {
            foreach (var textFile in LoadedTextFiles)
            {
                var tabPage = tabControl.TabPages.Cast<TabPage>().FirstOrDefault(page => page.Name.Equals(textFile.FileName));

                SetEditorStyle((Scintilla)tabPage.Controls[0], textFile.TextLanguage);
            }
        }

        private void SetWindowTitle()
        {
            this.Text = currentTextFile == null ? Consts.WindowTitle :
                    $"{(currentTextFile.IsModified ? "*" : "")}" +
                    $"{currentTextFile.ShortFileName} - {Consts.WindowTitle}";
        }

        private void ChangeZoom()
        {
            SettingsService.ZoomFactor = CurrentEditor.Zoom;
            lblZoomLevel.Text = $"Zoom: { (int)((float)SettingsService.ZoomFactor / (float)ThemeService.FontSize * 100.0f) + 100 }%";
        }

        private void SetLanguageDropDownValue()
        {
            if (currentTextFile != null)
            {
                cbLanguages.SelectedItem = cbLanguages.Items.Cast<TextLanguage>().FirstOrDefault(x => x.Language.Equals(currentTextFile.TextLanguage.Language));
            }
        }
        
        private TabPage GetClickedTab(MouseEventArgs e)
        {
            for (int i = 0; i < tabControl.TabPages.Count; i++)
            {
                // We need to iterate over every single tab page
                // and check if the mouse cursor position is inside
                // the tab header's rectangle.

                if (tabControl.GetTabRect(i).Contains(e.X, e.Y))
                {
                    return tabControl.TabPages[i];
                }
            }

            return null;
        }

        private void LoadWindowSettings()
        {
            if (SettingsService.WindowPosition.X > 0 && SettingsService.WindowPosition.Y > 0)
            {
                Location = SettingsService.WindowPosition;
            }

            Size = SettingsService.WindowSize;
            WindowState = SettingsService.IsMaximized ? FormWindowState.Maximized : FormWindowState.Normal;
        }

        private void SaveWindowSettings()
        {
            SettingsService.WindowPosition = Location;
            SettingsService.WindowSize = Size;
            SettingsService.IsMaximized = WindowState.Equals(FormWindowState.Maximized);
        }

        private void CloseSelectedTab(TabPage tabPage)
        {
            CloseEditorTab(tabPage);
        }

        private void LoadRecentFiles()
        {
            var recentFolder = SettingsService.OpenFolder;

            if (!string.IsNullOrEmpty(recentFolder))
            {
                OpenFolder(recentFolder);
            }

            var recentFiles = SettingsService.OpenFiles;

            if (recentFiles != null && recentFiles.Count > 0)
            {
                SettingsService.OpenFiles.ForEach(file => LoadTextFile(file));
                tabControl.SelectedIndex = SettingsService.OpenTabIndex;
            }

            if (filesToLoad != null && filesToLoad.Count > 0)
            {
                foreach (var file in filesToLoad)
                {
                    LoadTextFile(file);
                }
            }
        }

        private void SaveOpenFiles()
        {
            SettingsService.OpenFiles = (from file
                                         in LoadedTextFiles
                                         where !file.IsNew
                                         select file.FileName).ToList();

            SettingsService.OpenTabIndex = tabControl.SelectedIndex;
            SettingsService.OpenFolder = workspace == null ? string.Empty : workspace.Path;
        }

        private void Cut()
        {
            CurrentEditor.Cut();
        }

        private void Copy()
        {
            if (CurrentEditor.SelectedText.Length > 0)
            {
                CurrentEditor.Copy();
            }
        }

        private void Paste()
        {
            if (CurrentEditor.CanPaste)
            {
                CurrentEditor.Paste();
            }
        }

        private void Undo()
        {
            if (CurrentEditor.CanUndo)
            {
                if (currentTextFile.IsModified)
                {
                    CurrentEditor.Undo();
                    if (!CurrentEditor.CanUndo)
                    {
                        currentTextFile.IsModified = false;
                        UpdateFileStatus();
                    }
                }
                else
                {
                    CurrentEditor.Undo();
                    currentTextFile.IsModified = true;
                    UpdateFileStatus();
                }
            }
        }

        //TODO: test how it behaves and if also needs fixing like undo
        private void Redo()
        {
            if (CurrentEditor.CanRedo)
            {
                CurrentEditor.Redo();
            }
        }

        private void Delete()
        {
            int textLengthToDelete = CurrentEditor.SelectedText.Length == 0 ? 1 : CurrentEditor.SelectedText.Length;
            CurrentEditor.DeleteRange(CurrentEditor.CurrentPosition, textLengthToDelete);
        }

        private void SelectAll()
        {
            CurrentEditor.SelectAll();
        }

        private void SetWhitespaceVisibility()
        {
            CurrentEditor.ViewWhitespace = btnShowWhitespace.Checked ? WhitespaceMode.VisibleAlways : WhitespaceMode.Invisible;
        }

        private void SetIndentGuides()
        {
            CurrentEditor.IndentationGuides = btnShowIndentGuides.Checked ? IndentView.LookBoth : IndentView.None;
        }

        private void SetWordWrap()
        {
            CurrentEditor.WrapMode = btnWordWrap.Checked ? WrapMode.Word : WrapMode.None;
        }

        private void SetZoom()
        {
            CurrentEditor.Zoom = SettingsService.ZoomFactor;
        }

        private void ShowSearch(FindViewMode mode)
        {
            findView.Mode = mode;
            findView.Show();
            findView.Focus();
        }

        //TODO: Probably remove most key captures, since setting editor focus should have done the same job.
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Control | Keys.A:
                    SelectAll();
                    break;
                case Keys.Control | Keys.X:
                    Cut();
                    break;
                case Keys.Control | Keys.C:
                    Copy();
                    break;
                case Keys.Control | Keys.V:
                    Paste();
                    break;
                case Keys.Control | Keys.Z:
                    Undo();
                    break;
                case Keys.Control | Keys.F:
                    ShowSearch(FindViewMode.Find);
                    break;
                case Keys.Control | Keys.H:
                    ShowSearch(FindViewMode.FindAndReplace);
                    break;
                case Keys.Control | Keys.Shift | Keys.Z:
                    Redo();
                    break;
                case Keys.Delete:
                    Delete();
                    break;
                case Keys.F3:
                    Delete();
                    break;
                default:
                    break;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        #region Event Handlers

        private void btnNew_Click(object sender, EventArgs e)
        {
            LoadTextFile(null);
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadTextFile(openFileDialog.FileName);
            }
        }

        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                OpenFolder(folderBrowserDialog.SelectedPath);
            }
        }

        private void btnCloseFolder_Click(object sender, EventArgs e)
        {
            CloseFolder();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFile(currentTextFile, false);
            UpdateFileStatus();
        }

        private void btnSaveAll_Click(object sender, EventArgs e)
        {
            var filesToSave = LoadedTextFiles.Where(file => file.IsModified);

            foreach (var file in filesToSave)
            {
                SaveFile(file, false);
            }

            UpdateFileStatus();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabControl_MouseClick(object sender, MouseEventArgs e)
        {
            //Close on middle click
            if (e.Button == MouseButtons.Middle)
            {
                var tab = GetClickedTab(e);

                if (tab != null)
                {
                    CloseSelectedTab(tab);
                }
            }
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentTextFile = LoadedTextFiles.FirstOrDefault(
                x => x.FileName.Equals(tabControl.SelectedTab.Name));

            CurrentEditor?.Focus();
            findView.Editor = CurrentEditor;
            UpdateView();
            SetLanguageDropDownValue();
        }

        private void closeCurrentTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseSelectedTab(tabControl.SelectedTab);
        }

        private void Editor_TextChanged(object sender, EventArgs e)
        {
            if (currentTextFile.IsModified == true)
            {
                return;
            }

            currentTextFile.IsModified = true;
            tabControl.SelectedTab.ImageIndex = 1;

            btnSave.Enabled = true;
            btnSaveAll.Enabled = true;

            saveToolStripMenuItem.Enabled = true;
            saveAllToolStripMenuItem.Enabled = true;
            SetWindowTitle();
        }

        private void Editor_UpdateUI(object sender, UpdateUIEventArgs e)
        {
            UpdateView();
        }

        private void Editor_ZoomChanged(object sender, EventArgs e)
        {
            ChangeZoom();
        }

        private void Editor_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Editor_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files != null && files.Any())
            {
                foreach (var file in files)
                {
                    LoadTextFile(file);
                }
            }
        }

        private void closeAllTabsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TabPage tab in tabControl.TabPages)
            {
                CloseSelectedTab(tab);
            }
        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            Cut();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Copy();
            UpdateView();
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            Paste();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ShowSearch(FindViewMode.Find);
        }

        private void btnFindAndReplace_Click(object sender, EventArgs e)
        {
            ShowSearch(FindViewMode.FindAndReplace);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            Undo();
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
            Redo();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            CurrentEditor.ZoomIn();
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            CurrentEditor.ZoomOut();
        }

        private void btnShowWhitespace_CheckedChanged(object sender, EventArgs e)
        {
            SetWhitespaceVisibility();
            SettingsService.ShowWhiteSpace = btnShowWhitespace.Checked;
        }

        private void btnShowIndentGuides_Click(object sender, EventArgs e)
        {
            SetIndentGuides();
            SettingsService.ShowIndentGuides = btnShowIndentGuides.Checked;
        }

        private void btnWordWrap_Click(object sender, EventArgs e)
        {
            SetWordWrap();
            SettingsService.WordWrap = btnWordWrap.Checked;
        }

        private void settingsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var settingsView = new SettingsView();
            settingsView.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var aboutView = new AboutView();
            aboutView.ShowDialog();
        }

        private void cbLanguages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (currentTextFile.TextLanguage != null && cbLanguages.SelectedItem != null)
            {
                currentTextFile.TextLanguage = LanguageService.Languages.FirstOrDefault(x => x.Equals((TextLanguage)cbLanguages.SelectedItem));
                SetEditorStyle(CurrentEditor, currentTextFile.TextLanguage);
            }
        }

        private void EventService_SettingsChanged(object sender, EventArgs e)
        {
            UpdateAllEditorStyles();
        }

        private void EventService_FileOpened(object sender, Events.FileOpenedEventArgs e)
        {
            LoadTextFile(e.Path);
        }

        private void MainWindowView_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                findView.WindowState = FormWindowState.Minimized;
            }
            else if (WindowState == FormWindowState.Normal)
            {
                findView.WindowState = FormWindowState.Normal;
            }
        }

        #endregion
    }
}
