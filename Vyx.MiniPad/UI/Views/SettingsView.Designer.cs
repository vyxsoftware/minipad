﻿namespace Vyx.MiniPad.UI.Views
{
    partial class SettingsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsView));
            this.cbUseGlobalFontFamily = new System.Windows.Forms.CheckBox();
            this.lblFont = new System.Windows.Forms.Label();
            this.cbFont = new System.Windows.Forms.ComboBox();
            this.lblFontSize = new System.Windows.Forms.Label();
            this.cbFontSize = new System.Windows.Forms.ComboBox();
            this.cbUseGlobalFontSize = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cbUseGlobalFontFamily
            // 
            this.cbUseGlobalFontFamily.AutoSize = true;
            this.cbUseGlobalFontFamily.Location = new System.Drawing.Point(12, 12);
            this.cbUseGlobalFontFamily.Name = "cbUseGlobalFontFamily";
            this.cbUseGlobalFontFamily.Size = new System.Drawing.Size(126, 17);
            this.cbUseGlobalFontFamily.TabIndex = 0;
            this.cbUseGlobalFontFamily.Text = "Use global font family";
            this.cbUseGlobalFontFamily.UseVisualStyleBackColor = true;
            this.cbUseGlobalFontFamily.CheckedChanged += new System.EventHandler(this.cbUseGlobalFontSettings_CheckedChanged);
            // 
            // lblFont
            // 
            this.lblFont.AutoSize = true;
            this.lblFont.Location = new System.Drawing.Point(14, 39);
            this.lblFont.Name = "lblFont";
            this.lblFont.Size = new System.Drawing.Size(31, 13);
            this.lblFont.TabIndex = 1;
            this.lblFont.Text = "Font:";
            // 
            // cbFont
            // 
            this.cbFont.FormattingEnabled = true;
            this.cbFont.Location = new System.Drawing.Point(98, 36);
            this.cbFont.Name = "cbFont";
            this.cbFont.Size = new System.Drawing.Size(121, 21);
            this.cbFont.TabIndex = 2;
            this.cbFont.SelectedIndexChanged += new System.EventHandler(this.cbFont_SelectedIndexChanged);
            // 
            // lblFontSize
            // 
            this.lblFontSize.AutoSize = true;
            this.lblFontSize.Location = new System.Drawing.Point(14, 89);
            this.lblFontSize.Name = "lblFontSize";
            this.lblFontSize.Size = new System.Drawing.Size(52, 13);
            this.lblFontSize.TabIndex = 4;
            this.lblFontSize.Text = "Font size:";
            // 
            // cbFontSize
            // 
            this.cbFontSize.FormattingEnabled = true;
            this.cbFontSize.Location = new System.Drawing.Point(98, 86);
            this.cbFontSize.Name = "cbFontSize";
            this.cbFontSize.Size = new System.Drawing.Size(121, 21);
            this.cbFontSize.TabIndex = 5;
            this.cbFontSize.SelectedIndexChanged += new System.EventHandler(this.cbFontSize_SelectedIndexChanged);
            // 
            // cbUseGlobalFontSize
            // 
            this.cbUseGlobalFontSize.AutoSize = true;
            this.cbUseGlobalFontSize.Location = new System.Drawing.Point(12, 63);
            this.cbUseGlobalFontSize.Name = "cbUseGlobalFontSize";
            this.cbUseGlobalFontSize.Size = new System.Drawing.Size(118, 17);
            this.cbUseGlobalFontSize.TabIndex = 6;
            this.cbUseGlobalFontSize.Text = "Use global font size";
            this.cbUseGlobalFontSize.UseVisualStyleBackColor = true;
            this.cbUseGlobalFontSize.CheckedChanged += new System.EventHandler(this.cbUseGlobalFontSize_CheckedChanged);
            // 
            // SettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 125);
            this.Controls.Add(this.cbUseGlobalFontSize);
            this.Controls.Add(this.cbFontSize);
            this.Controls.Add(this.lblFontSize);
            this.Controls.Add(this.cbFont);
            this.Controls.Add(this.lblFont);
            this.Controls.Add(this.cbUseGlobalFontFamily);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbUseGlobalFontFamily;
        private System.Windows.Forms.Label lblFont;
        private System.Windows.Forms.ComboBox cbFont;
        private System.Windows.Forms.Label lblFontSize;
        private System.Windows.Forms.ComboBox cbFontSize;
        private System.Windows.Forms.CheckBox cbUseGlobalFontSize;
    }
}