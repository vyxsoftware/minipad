﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;

namespace Vyx.MiniPad.UI.Views
{
    public partial class SettingsView : FormBase
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        protected override void InitializeView()
        {
            using (var fontCollection = new InstalledFontCollection())
            {
                foreach (FontFamily font in fontCollection.Families)
                {
                    cbFont.Items.Add(font.Name);
                }
                cbFont.SelectedItem = SettingsService.GlobalFont;

                int[] fontSizes = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 22, 24, 26, 28 };

                foreach (int fontSize in fontSizes)
                {
                    cbFontSize.Items.Add(fontSize);
                }
                cbFontSize.SelectedItem = SettingsService.GlobalFontSize;
            }

            cbUseGlobalFontFamily.Checked = SettingsService.GlobalFontEnabled;
            cbUseGlobalFontSize.Checked = SettingsService.GlobalFontSizeEnabled;
            UpdateView();
        }

        private void UpdateView()
        {
            cbFont.Enabled = cbUseGlobalFontFamily.Checked;
            cbFontSize.Enabled = cbUseGlobalFontSize.Checked;
        }

        private void cbUseGlobalFontSettings_CheckedChanged(object sender, EventArgs e)
        {
            SettingsService.GlobalFontEnabled = cbUseGlobalFontFamily.Checked;
            UpdateView();
            EventService.OnSettingsChanged(new EventArgs());
        }

        private void cbUseGlobalFontSize_CheckedChanged(object sender, EventArgs e)
        {
            SettingsService.GlobalFontSizeEnabled = cbUseGlobalFontSize.Checked;
            UpdateView();
            EventService.OnSettingsChanged(new EventArgs());
        }

        private void cbFont_SelectedIndexChanged(object sender, EventArgs e)
        {
            SettingsService.GlobalFont = (string)cbFont.SelectedItem;
            EventService.OnSettingsChanged(new EventArgs());
        }

        private void cbFontSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            SettingsService.GlobalFontSize = (int)cbFontSize.SelectedItem;
            EventService.OnSettingsChanged(new EventArgs());
        }
    }
}
