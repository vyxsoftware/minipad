﻿namespace Vyx.MiniPad.UI.Views.Controls
{
    partial class FolderView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderView));
            this.tvFolders = new System.Windows.Forms.TreeView();
            this.folderIcons = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // tvFolders
            // 
            this.tvFolders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvFolders.ImageIndex = 0;
            this.tvFolders.ImageList = this.folderIcons;
            this.tvFolders.Location = new System.Drawing.Point(0, 0);
            this.tvFolders.Name = "tvFolders";
            this.tvFolders.SelectedImageIndex = 0;
            this.tvFolders.Size = new System.Drawing.Size(150, 150);
            this.tvFolders.TabIndex = 0;
            this.tvFolders.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvFolders_BeforeExpand);
            this.tvFolders.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvFolders_NodeMouseDoubleClick);
            // 
            // folderIcons
            // 
            this.folderIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("folderIcons.ImageStream")));
            this.folderIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.folderIcons.Images.SetKeyName(0, "hourglass.png");
            this.folderIcons.Images.SetKeyName(1, "folder.png");
            this.folderIcons.Images.SetKeyName(2, "page_white_text.png");
            this.folderIcons.Images.SetKeyName(3, "page_white.png");
            this.folderIcons.Images.SetKeyName(4, "page_white_asm.png");
            this.folderIcons.Images.SetKeyName(5, "page_white_basic.png");
            this.folderIcons.Images.SetKeyName(6, "application_bat.png");
            this.folderIcons.Images.SetKeyName(7, "page_white_c.png");
            this.folderIcons.Images.SetKeyName(8, "page_white_cplusplus.png");
            this.folderIcons.Images.SetKeyName(9, "page_white_cs.png");
            this.folderIcons.Images.SetKeyName(10, "page_white_braces.png");
            this.folderIcons.Images.SetKeyName(11, "page_white_f.png");
            this.folderIcons.Images.SetKeyName(12, "page_white_h.png");
            this.folderIcons.Images.SetKeyName(13, "page_white_world.png");
            this.folderIcons.Images.SetKeyName(14, "page_white_cup.png");
            this.folderIcons.Images.SetKeyName(15, "page_white_js.png");
            this.folderIcons.Images.SetKeyName(16, "page_white_braces.png");
            this.folderIcons.Images.SetKeyName(17, "page_white_l.png");
            this.folderIcons.Images.SetKeyName(18, "page_white_l.png");
            this.folderIcons.Images.SetKeyName(19, "page_white_p.png");
            this.folderIcons.Images.SetKeyName(20, "page_white_p.png");
            this.folderIcons.Images.SetKeyName(21, "page_white_php.png");
            this.folderIcons.Images.SetKeyName(22, "page_white_ps.png");
            this.folderIcons.Images.SetKeyName(23, "page_white_gear.png");
            this.folderIcons.Images.SetKeyName(24, "page_white_basic.png");
            this.folderIcons.Images.SetKeyName(25, "page_white_python.png");
            this.folderIcons.Images.SetKeyName(26, "page_white_ruby.png");
            this.folderIcons.Images.SetKeyName(27, "page_white.png");
            this.folderIcons.Images.SetKeyName(28, "page_white_database.png");
            this.folderIcons.Images.SetKeyName(29, "page_white_vb.png");
            this.folderIcons.Images.SetKeyName(30, "script.png");
            this.folderIcons.Images.SetKeyName(31, "page_white.png");
            this.folderIcons.Images.SetKeyName(32, "page_white_code.png");
            this.folderIcons.Images.SetKeyName(33, "page_white_code.png");
            this.folderIcons.Images.SetKeyName(34, "page_white_basic.png");
            this.folderIcons.Images.SetKeyName(35, "page_white.png");
            this.folderIcons.Images.SetKeyName(36, "page_white.png");
            // 
            // FolderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tvFolders);
            this.Name = "FolderView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvFolders;
        private System.Windows.Forms.ImageList folderIcons;
    }
}
