﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;

namespace Vyx.MiniPad.UI.Views.Controls
{
    public partial class WelcomeView : ControlBase
    {
        public event Action<object, EventArgs> NewFile;
        public event Action<object, EventArgs> OpenFile;
        public event Action<object, EventArgs> OpenFolder;

        public WelcomeView()
        {
            InitializeComponent();
        }

        private void OnNewFile()
        {
            NewFile?.Invoke(this, new EventArgs());
        }

        private void OnOpenFile()
        {
            OpenFile?.Invoke(this, new EventArgs());
        }

        private void OnOpenFolder()
        {
            OpenFolder?.Invoke(this, new EventArgs());
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            OnNewFile();
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OnOpenFile();
        }

        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            OnOpenFolder();
        }
    }
}
