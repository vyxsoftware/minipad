﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vyx.MiniPad.Common;
using Vyx.MiniPad.Events;
using Vyx.MiniPad.Models;

namespace Vyx.MiniPad.UI.Views.Controls
{
    public partial class FolderView : ControlBase
    {
        private TreeNode folderTree;

        public TreeNode FolderTree
        {
            get => folderTree;
            set
            {
                folderTree = value;

                if (value != null)
                {
                    tvFolders.Nodes.Clear();
                    tvFolders.Nodes.Add(FolderTree);
                    tvFolders.Nodes[0].Expand();
                }
            }
        }

        public FolderView()
        {
            InitializeComponent();
        }

        private void tvFolders_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var path = ((FileTreeNode)e.Node.Tag).Path;

            if (File.Exists(path))
            {
                EventService.OnFileOpened(new FileOpenedEventArgs { Path = path });
            }
        }

        private void tvFolders_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            var node = e.Node as TreeNode;

            if (node == null)
            {
                return;
            }

            if (node.Nodes.Cast<TreeNode>().Any(
                x => x.Tag.GetType().Equals(typeof(FileTreeNode))
                && ((FileTreeNode)x.Tag).NodeType.Equals(FileTreeNodeType.Dummy)))
            {
                var children = FileService.GetChildren(((FileTreeNode)node.Tag).Path);
                node.Nodes.Clear();
                node.Nodes.AddRange(children.ToArray<TreeNode>());
            }
        }
    }
}
