﻿using ScintillaNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Vyx.MiniPad.Tools.StyleGen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var style = new Style(null, 0);

            XmlDocument document = new XmlDocument();
            XmlDeclaration xmlDeclaration = document.CreateXmlDeclaration("1.0", "UTF-8", null);

            XmlElement root = document.DocumentElement;
            document.InsertBefore(xmlDeclaration, root);

            // Main
            XmlElement main = document.CreateElement(string.Empty, "MiniPadStyle", string.Empty);
            main.SetAttribute("Name", "Default");
            document.AppendChild(main);

            //Fonts
            XmlElement fontElement = document.CreateElement(string.Empty, "Font", string.Empty);
            fontElement.SetAttribute("Name", "Consolas");
            fontElement.SetAttribute("Size", "10");
            main.AppendChild(fontElement);

            //Colors
            XmlElement colorsElement = document.CreateElement(string.Empty, "Colors", string.Empty);

            XmlElement colorElement1 = document.CreateElement(string.Empty, "Color", string.Empty);
            colorElement1.SetAttribute("Name", "White");
            colorElement1.SetAttribute("Value", "#FFFFFF");
            colorsElement.AppendChild(colorElement1);

            XmlElement colorElement2 = document.CreateElement(string.Empty, "Color", string.Empty);
            colorElement2.SetAttribute("Name", "Black");
            colorElement2.SetAttribute("Value", "#000000");
            colorsElement.AppendChild(colorElement2);

            main.AppendChild(colorsElement);

            //Styles for each language type
            XmlElement langElements = document.CreateElement(string.Empty, "Languages", string.Empty);

            //Default
            XmlElement defaultLangElement = document.CreateElement(string.Empty, "Language", string.Empty);
            defaultLangElement.SetAttribute("Name", "Text");

            XmlElement defaultStyleElement = document.CreateElement(string.Empty, "Style", string.Empty);

            defaultStyleElement.SetAttribute("Name", "Default");
            defaultStyleElement.SetAttribute("Id", "0");
            defaultStyleElement.SetAttribute("ForegroundColor", "Black");
            defaultStyleElement.SetAttribute("BackgroundColor", "White");
            defaultStyleElement.SetAttribute("FontStyle", "Regular");

            defaultLangElement.AppendChild(defaultStyleElement);
            langElements.AppendChild(defaultLangElement);

            var langs = style.GetType().GetNestedTypes();

            foreach (TypeInfo lang in langs)
            {
                XmlElement langElement = document.CreateElement(string.Empty, "Language", string.Empty);
                langElement.SetAttribute("Name", lang.Name);

                // Styles
                foreach (var item in lang.DeclaredFields)
                {
                    XmlElement styleElement = document.CreateElement(string.Empty, "Style", string.Empty);

                    styleElement.SetAttribute("Name", item.Name);
                    styleElement.SetAttribute("Id", item.GetValue(style).ToString());
                    styleElement.SetAttribute("ForegroundColor", "Black");
                    styleElement.SetAttribute("BackgroundColor", "White");
                    styleElement.SetAttribute("FontStyle", "Regular");

                    langElement.AppendChild(styleElement);
                }

                langElements.AppendChild(langElement);
            }
            main.AppendChild(langElements);

            document.Save(Directory.GetCurrentDirectory() + "\\Styles\\Default.xml");
        }
    }
}
